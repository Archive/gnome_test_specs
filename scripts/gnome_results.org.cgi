#!/usr/bin/ksh
#
# This CGI-BIN script records the results of GNOME tests in a
# HTML file, and displays the file
#
#

#
# HTTP headers
#
print -u1 "Content-type: text/html\n\n"

#
# location to save results .html file
#
DOCUMENT_ROOT="../docs"
BASE_DIR="${DOCUMENT_ROOT}/test/gnome_test"

SCRIPT="`basename $0`"
INPUT_FROM_FORM="/tmp/$SCRIPT.tmp.$$"
REPORT_TEXT_BODY="/tmp/${SCRIPT}.tmp.2.$$"
DATE="`date +%a` `date +%b` `date +%e`, `date +%Y` `date +%T` `date +%p` `date +%Z`"
integer num_tests=0 num_passed=0 num_failed=0 num_not_done=0 perc_passed=0

#
# Convert the variable input of the HTML form from CGI
# to a more convenient form.
#
CGI_PAIRS="`cat`"
SAVED_IFS="$IFS" ; IFS="&" ; set -- $CGI_PAIRS
for pair in $*
do
	print -u1 "$pair" 
done > $INPUT_FROM_FORM
IFS="$SAVED_IFS"

touch $REPORT_TEXT_BODY

#
# Read input and take action or set conditions accordingly.
#
while read -r
do
	name="`echo $REPLY | awk -F'=' '{ print $1 }'`"
        value="`echo $REPLY | awk -F'=' '{ print $2 }' | tr '+' ' '`"
	#
        # Convert spaces to underscores for any fields input by user
        # that become part of filenames, ie, tester and machine.
        #
        case $name in
		result_*)
			TEST_NAME="`echo $name | sed -e 's/^result_//'`"

			print -u1 "<TR><TD>${TEST_NAME}</TD><TD>${value}</TD></TR>" \
				>> $REPORT_TEXT_BODY

			case $value in
				"passed") let num_passed=num_passed+1 ;;
				"failed") let num_failed=num_failed+1 ;;
				"not done") let num_not_done=num_not_done+1 ;;
			esac
			;;
		BUILD_ID)
			BUILD_ID=$value
			;;
		PLATFORM)
			PLATFORM=$value
			;;
		TESTER)
			TESTER=$value
			;;
		MAJOR_TEST_ID)
			MAJOR_TEST_ID=$value
			;;	
		MINOR_TEST_ID)
			MINOR_TEST_ID=$value
			;;	
	esac
done < $INPUT_FROM_FORM

BUILD_ID="`echo $BUILD_ID | sed -e 's/ /_/'`"
if [ $BUILD_ID = "" ] ; then
	BUILD_ID="dflt"
fi

PLATFORM="`echo $PLATFORM | sed -e 's/ /_/'`"
if [ $PLATFORM = "" ] ; then
	PLATFORM="dlft"
fi

TESTER="`echo $TESTER | sed -e 's/ /_/'`"
if [ $TESTER = "" ] ; then
	TESTER="nobody"
fi

if [ $MAJOR_TEST_ID = "" ] ; then
	MAJOR_TEST_ID="unknown"
fi

if [ $MINOR_TEST_ID = "" ] ; then
	MINOR_TEST_ID="unknown"
fi


RESULTS_DIR="${BASE_DIR}/results/${PLATFORM}"
if [ ! -d $RESULTS_DIR ] ; then
	mkdir $RESULTS_DIR
fi

RESULTS_DIR="${RESULTS_DIR}/${BUILD_ID}"
if [ ! -d $RESULTS_DIR ] ; then
	mkdir $RESULTS_DIR
fi

RESULTS_DIR="${RESULTS_DIR}/${MAJOR_TEST_ID}"
if [ ! -d $RESULTS_DIR ] ; then
	mkdir $RESULTS_DIR
fi

HTML_REPORT="${RESULTS_DIR}/${MINOR_TEST_ID}_${TESTER}.html"

let num_tests=num_passed+num_failed+num_not_done
(( perc_passed=num_passed*100/num_tests ))
(( perc_failed=num_failed*100/num_tests ))
(( perc_done=(num_failed+num_passed)*100/num_tests ))

#
# Save report of test results to a .html file, prepending header and other
# general info, and then display contents of the results file
#
for loop in once
do
        print -u1 "<HTML><HEAD><TITLE>TEST RESULT: $MAJOR_TEST_ID $MINOR_TEST_ID</TITLE></HEAD>"
        print -u1 "<BODY BGCOLOR=ffffff>"
        print -u1 "<H1 ALIGN=center>Test Results</H1>"
	print -u1 "<B>Test Id:</B> $MAJOR_TEST_ID/$MINOR_TEST_ID<BR>"
	print -u1 "<B>Build Number:</B> $BUILD_ID<BR>"	
	print -u1 "<B>Platform:</B> $PLATFORM<BR>"
	print -u1 "<B>Tester:</B> $TESTER<BR>"	
        print -u1 "<B>Date Test Run:</B> $DATE<BR>"
        print -u1 "<B>Results saved in:</B> $HTML_REPORT<BR>"
	print -u1 "<P>"
	print -u1 "<CENTER><TABLE BORDER=2 CELLPADDING=2 CELLSPACING=2>"
	print -u1 "<CAPTION><B>Test Summary</B></CAPTION>"
        print -u1 "<TR><TD>Number of tests run</TD>"
	print -u1 "<TD>$num_tests</TD></TR>"
        print -u1 "<TR><TD>Number of tests passed</TD>"
	print -u1 "<TD>$num_passed</TD></TR>"
        print -u1 "<TR><TD>Number of tests failed</TD>"
	print -u1 "<TD>$num_failed</TD></TR>"
        print -u1 "<TR><TD>Number of tests not done</TD>"
	print -u1 "<TD>$num_not_done</TD></TR>"
        print -u1 "<TR><TD>Percentage of Tests Passed</TD>"
	print -u1 "<TD>$perc_passed</TD></TR>"
        print -u1 "<TR><TD>Percentage of Tests Failed</TD>"
	print -u1 "<TD>$perc_failed</TD></TR>"
        print -u1 "<TR><TD>Percentage of Tests Executed</TD>"
	print -u1 "<TD>$perc_done</TD></TR>"
        print -u1 "</TABLE></CENTER>"
        print -u1 "<BR><HR><BR>"

	print -u1 "<CENTER><TABLE BORDER=2 CELLPADDING=2 CELLSPACING=2>"
	print -u1 "<CAPTION><B>Test Results</B></CAPTION>"
        print -u1 "<TR><TH>Test name</TH><TH>Test result</TH></TR>"
        cat $REPORT_TEXT_BODY
        print -u1 "</TABLE>"
        print -u1 "</BODY></HTML>"
done > $HTML_REPORT

cat $HTML_REPORT

# clean up
/bin/rm -f $INPUT_FROM_FORM $REPORT_TEXT_BODY
