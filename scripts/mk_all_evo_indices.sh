#!/bin/sh -x 

base_dir=".."
dir="$base_dir/evolution"
script="../../../scripts/make_index.pl"

cd $dir
cd calendar
cd cal_time; $script evolution cal_time; cd ..
cd cal_create; $script evolution cal_create; cd ..
cd cal_edit; $script evolution cal_edit; cd ..
cd cal_file; $script evolution cal_file; cd ..
cd cal_gen; $script evolution cal_gen; cd ..
cd cal_help; $script evolution cal_help; cd ..
cd cal_print; $script evolution cal_print; cd ..
cd cal_task; $script evolution cal_task; cd ..
cd cal_shortcut; $script evolution cal_shortcut; cd ..
cd cal_actions; $script evolution cal_actions; cd ..
cd cal_tools; $script evolution cal_tools; cd ..
cd cal_view; $script evolution cal_view; cd ..
cd ..
cd myevol
cd myevol_gen; $script evolution myevol_gen; cd ..
cd myevol_file; $script evolution myevol_file; cd ..
cd myevol_tools; $script evolution myevol_tools; cd ..
cd myevol_view; $script evolution myevol_view; cd ..
cd myevol_help; $script evolution myevol_help; cd ..
cd ..
cd tasks
cd tasks_gen; $script evolution tasks_gen; cd ..
cd tasks_file; $script evolution tasks_file; cd ..
cd tasks_edit; $script evolution tasks_edit; cd ..
cd tasks_view; $script evolution tasks_view; cd ..
cd tasks_tools; $script evolution tasks_tools; cd ..
cd tasks_help; $script evolution tasks_help; cd ..
cd ..
cd contacts
cd con_edit; $script evolution con_edit; cd ..
cd con_file; $script evolution con_file; cd ..
cd con_gen; $script evolution con_gen; cd ..
cd con_help; $script evolution con_help; cd ..
cd con_new; $script evolution con_new; cd ..
cd con_shortcut; $script evolution con_shortcut; cd ..
cd con_toolbar; $script evolution con_toolbar; cd ..
cd con_tools; $script evolution con_tools; cd ..
cd con_alter; $script evolution con_alter; cd ..
cd con_view; $script evolution con_view; cd ..
cd ..
cd mail
cd mail_comp; $script evolution mail_comp; cd ..
cd mail_conf; $script evolution mail_conf; cd ..
cd mail_edit; $script evolution mail_edit; cd ..
cd mail_file; $script evolution mail_file; cd ..
cd mail_filter; $script evolution mail_filter; cd ..
cd mail_folder; $script evolution mail_folder; cd ..
cd mail_gen; $script evolution mail_gen; cd ..
cd mail_help; $script evolution mail_help; cd ..
cd mail_message; $script evolution mail_message; cd ..
cd mail_search; $script evolution mail_search; cd ..
cd mail_actions; $script evolution mail_actions; cd ..
cd mail_tools; $script evolution mail_tools; cd ..
cd mail_shortcut; $script evolution mail_shortcut; cd ..
cd mail_tool; $script evolution mail_tool; cd ..
cd mail_virt; $script evolution mail_virt; cd ..
cd mail_view; $script evolution mail_view; cd ..
cd ..
cd regression
cd latest; $script evolution latest; cd ..
cd ..

script="../../scripts/make_index.pl"

cd hotkeys; $script evolution hotkey; cd ..
cd int; $script evolution int; cd ..

find . -name index.html -exec chmod 755 {} \;
