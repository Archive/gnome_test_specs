#!/usr/bin/perl -w

# This script is designed to test the scrollkeeper component version-0.3 on gnome-1.4
# by Keelin Boyle

# This script assumes system has built and installed scrollkeeper using the /opt/gnome-1.4-debug prefix 

######Global variables############
my ($con_list );
my ($ex_con_list);
my ($prefix);
my ($set_var);
my ($old_omf_dir);
my ($omf_dir);
my ($doc_path);
my ($ref_files);
#################################
$comp_dir1 = "gnome-utils";
$comp_dir2 = "gnome-core";
$prefix ="/opt/gnome-1.4-debug";
$old_omf_dir ="$prefix/share/omf";
$res_dir = "/tmp";
$new_omf = "test_omf";
$doc_path = "/$prefix/share/gnome/help";
$ref_files = "/scde/web/docs/test/gnome_test/components/scrollkeeper";

system ("cd $res_dir"); 
system ("rm -rf $res_dir/$new_omf");
system ("mkdir $res_dir/$new_omf");
system ("chmod 777 $res_dir/$new_omf");
#$OMF_DIR=$res_dir/$new_omf");
$set_var = $ENV{"OMF_DIR"};

##############################################################################
sub get_contents_list ()
{

#getting the xml file contents_list

$con_list = `$prefix/bin/scrollkeeper-get-content-list C`;
#print "con_list = $con_list\n\n";
return $con_list;
} 
##############################################################################
sub get_ex_contents_list ()
{

#getting the xml file extended_contents_list

$ex_con_list = `$prefix/bin/scrollkeeper-get-extended-content-list C`;
#system ("$prefix/bin/scrollkeeper-get-toc-from-docpath $doc_path/panel/C/panel.sgml");
#print "ex_con_list = $ex_con_list\n\n";
return $ex_con_list;
}

##############################################################################
sub get_toc ()
{

#$get_toc = `$prefix/bin/scrollkeeper-get-toc-from-docpath $doc_path/panel/C/panel.sgml`;
$get_toc = `$prefix/bin/scrollkeeper-get-toc-from-docpath /opt/gnome*/share/gnome/help/panel/C/panel.sgml`;
print "toc from docpath = $get_toc\n\n";
return $get_toc;
}

##############################################################################
sub check_contents_list() 
{
	my ($con_list) = @_;

#print "In sub dir 2 searching $con_list\n";
print "\nCONTENTS LIST\n";

#checking wheather any entries have been made to .xml files
#$scrollkeeper_db_dir ="$prefix/var/lib/scrollkeeper/C";
$templates = "$prefix/share/scrollkeeper/Templates/C";
#compare the xml content list template and the new content list 
#chomp $con_list;
#if (`cmp "$con_list" "$templates/scrollkeeper_cl.xml"`) {
#	print "The database rebuild has failed\n\n"; }
#      else {
        #print "The database rebuild was successful\n\n";
        #}						 
         


# open the scrollkeeper database and check contents list xml file for installed doc entries \n";
open (COL, "<$con_list") or die "Can't open file $con_list\n";
		$count =0;
		while (<COL>) {
                            chomp;
                            $line =$_;
                            # these match the line entries for installed docs
                            if ($line =~ /(doctitle.+doctitle)/){
                            #if ($line =~ m/Manual/i) {
			       $count = $count +1; 
                               print "Entry for doc $count found in Contents List ";
                               print "$1\n";
					        	}
			     }
print "\nContents List has $count document entries\n\n";

#compare the xml content list reference file with the newly created content list

#print "$ref_files\n";
print $con_list;

chomp $con_list;
# the cmp is TRUE if the files are different .i.e. = 1 and 0 if they are identical
# if the files differ print a message and print the difference to /tmp/diff
if (`cmp "$con_list" "$ref_files/con_list_ref"`) {
            print "\nAn error has occurred and the contents list is inaccurate.\n\n"; 
            unlink "/tmp/diff";
            `diff  "$con_list" "$ref_files/con_list_ref" > /tmp/diff`; } 
	else {
            print "\nThe contents list has been created sucessfully\n\n";                     
	 }                                      

#unlink "/tmp/diff";
#system ("diff $con_list  $templates/scrollkeeper_cl.xml > /tmp/diff");

return ;
}

###############################################################################
sub check_ex_contents_list ()
{
	my ($ex_con_list) = @_;

#print "Hi in sub dir 3 searching $ex_con_list\n";
print "\nEXTENDED CONTENTS LIST\n";

#checking wheather any entries have been made to extended contents list .xml file
#$scrollkeeper_db_dir ="$prefix/var/lib/scrollkeeper/C";
$templates = "$prefix/share/scrollkeeper/Templates/C";
#compare the xml content list template and the new content list
#if (system ("cmp $ex_con_list  $templates/scrollkeeper_cl.xml") == 0) {
#	print "The database rebuild has failed\n";}


# open the scrollkeeper database and check contents list xml file for installed doc entries


open (ECL, "<$ex_con_list") or die "Can't open file $ex_con_list\n";
		  $count =0;
		while (<ECL>) {
                            chomp;
                            $line =$_;
		            # these match the line entries for installed docs
                            if ($line =~ /(doctitle.+doctitle)/){
			    #if ($line =~ m/Manual/i) {
                            $count = $count +1; 
		            print "Entry for doc $count found in Extended_contents List";
                            print "$1\n";
					 }
		}
print "\nExtended Contents List has $count document entries\n\n";

chomp $ex_con_list;
# the cmp is TRUE if the files are different .i.e. = 1 and 0 if they are identical
# if the files differ print the difference to /tmp/diff_2
if (`cmp "$ex_con_list" "$ref_files/ex_con_list_ref"`) {
	print "\nAn error has occurred and the extended contents list is inaccurate.\n\n"; 
        unlink "/tmp/diff_2";
        `diff  "$ex_con_list" "$ref_files/ex_con_list_ref" > /tmp/diff_2`; } 
	else {
	print "\nThe extended contents list has been created sucessfully\n\n";
	}

#unlink "/tmp/diff_2";
# (`diff "$ex_con_list" "$templates/scrollkeeper_cl.xml > /tmp/diff_2");

return ;
}


##############################################################################
sub omf_dir_count()
{
print "\nOMF DIRECTORY\n";
# open the omf directory and count # of files.

print "Opening omf_dir\n";
$omf_dir = "/tmp/test_omf";
$count =0;
opendir (OMF_DIRS, $omf_dir);
@DIR_LIST = readdir (OMF_DIRS);
foreach $item (@DIR_LIST) {
	if ($item =~ m/.omf/i) {
		print "The metadata file: $item is installed\n";
		$count =$count +1;
        			}

                           }

print "\nThere are currently $count metadata files in $omf_dir\n\n";
}
##############################################################################
sub toc_index_count()
{
print "\nINDEX DIRECTORY\n";
# open the TOC directory and count # of indexes.

$toc_path = "$prefix/var/lib/scrollkeeper/TOC";
print "Opening toc_dir\n";
$count =0;
opendir (TOC_DIR, $toc_path);
@DIR_LIST = readdir (TOC_DIR);
foreach $item (@DIR_LIST) {
	if ($item =~ /\d/) {
			print "Index $item found\n";
			$count =$count +1;
                                }

                           }

print "There are currently $count index files in $toc_path\n\n";
}

##############################################################################
#open the installed omf dir, copy omf files gcalc, fish applet & panel.

#print "omf dir is $omf_dir\n";
$omf_util = "$old_omf_dir/$comp_dir1";
$omf_core ="$old_omf_dir/$comp_dir2";

 print "Copying omf input files from gnome utilities to /tmp/test_omf \n";
	    system ("cd $omf_util; cp $omf_util/gcalc-C.omf $res_dir/$new_omf");
	    system ("cd $omf_core; cp $omf_core/fish_applet-C.omf $res_dir/$new_omf");
	    system ("cd $omf_core; cp $omf_core/panel-C.omf $res_dir/$new_omf");


#Run scrollkeeper-rebuilddb to rebuild database with only 3 specified docs .i.e. those with omf files.
print "\nRunning scrollkeeper-rebuilddb \n";
system ("$prefix/bin/scrollkeeper-rebuilddb");

print "\nTest 1: Check Contents & Extended Con List's doc entries match metadata files in the omf dir\n";  
&omf_dir_count();
&toc_index_count();
&get_contents_list ();
&check_contents_list($con_list);
&get_ex_contents_list ();
&check_ex_contents_list($ex_con_list);
&get_toc;
system ("sleep 10");

print "Test 2: Remove a metadata file, run scrollkeeper update & check Con List's\n";  

#testing removal of an omf file and scrollkeeper-update.
print "\nRemoving panel omf file and running scrollkeeper-update.\n";
system ("rm -rf $omf_dir/panel-C.omf");
system ("$prefix/bin/scrollkeeper-update");

&omf_dir_count();
&toc_index_count();
&get_contents_list ();
&check_contents_list($con_list);
&get_ex_contents_list ();
&check_ex_contents_list($ex_con_list);
system ("sleep 10");

print "Test 3: Add a metadata file, run scrollkeeper update & check Con List's\n"; 

#testing adding an omf file and scrollkeeper_update
print "\nAdding panel omf file and running scrollkeeper-update.\n";
system ("cd $omf_core; cp $omf_core/panel-C.omf $res_dir/$new_omf");
system ("$prefix/bin/scrollkeeper-update");

&omf_dir_count();
&toc_index_count();
&get_contents_list ();
&check_contents_list($con_list);
&get_ex_contents_list ();
&check_ex_contents_list($ex_con_list);
system ("sleep 10");


print "\n\n$set_var\n";
