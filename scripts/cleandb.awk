#!/usr/bin/nawk 
BEGIN{
FS="@@@"
TMP=TMP 
POS=0
Y=X=0  
TMP_DONE="NO" 
while ((getline CURRENT < TMP) > 0 )
	{
        if (CURRENT != "") { TMP_RECORD[X] = CURRENT 
				X++ } 
	}
}
{
if ($1 != "") { DB_RECORD[Y]=$1FS$2FS$3FS$4FS$5FS$6      		#ignore blank lines 
		Y++ }  
} 
END{
for ( y = 0 ; y < Y ; y++ )
	{
	split(DB_RECORD[y], DB_FIELDS)
	POS=index(TMP_RECORD[0], DB_FIELDS[3])
		if ((POS != "0" )  && (TMP_DONE == "NO"))
		{
		for ( x = 0 ; x < X ; x++ ) { print TMP_RECORD[x]
						TMP_DONE="YES" }   
		}
	if (POS == 0 ) { print DB_RECORD[y] } 
	}
if (TMP_DONE == "NO")   # no matches found output TMP  
	    { for ( x = 0 ; x < X ; x++ ) { print TMP_RECORD[x]} } 
				
}

