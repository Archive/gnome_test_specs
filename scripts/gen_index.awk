#!/usr/bin/nawk 
BEGIN{
FS="/"
X=1
FLAG="N_DONE" 
print "<HTML>"
print "<BODY BGCOLOR=\"#ffffff\">"
print "<P ALIGN=CENTER STYLE=\"margin-bottom: 0in\"><B>Test Results Index "MAJOR"</B></P>"
print "<BR>"
print "<CENTER>"
print "<TABLE WIDTH=747 BORDER=1 CELLPADDING=4 CELLSPACING=3>"
print "<COL WIDTH=158>"
print "<COL WIDTH=170>"
print "<COL WIDTH=129>"
print "<COL WIDTH=241>"
print "<THEAD>"
print "<TR VALIGN=TOP>"
print "<TH WIDTH=158>"
print "<P ALIGN=CENTER>PLATFORM</P>"
print "</TH>"
print "<TH WIDTH=170>"
print "<P ALIGN=CENTER>OS_VERSION</P>"
print "</TH>"
print "<TH WIDTH=129>"
print "<P ALIGN=CENTER>BUILD_ID</P>"
print "</TH>"
print "<TH WIDTH=241>"
print "<P ALIGN=CENTER>RESULTS</P>"
print "</TH>"
print "</TR>"
print "</THEAD>"
print "<TBODY>"
}
# generates index table sorted on build id's of product
{
if ($1 != "")                           		#ignore blank lines 
	{ 
	for ( Y=1 ; Y<10 ; Y++)
		{
		FIELD[X,Y]=$Y 
		}
	X++
	}
} 
END{
for ( x=1 ; x < X-1 ; x++ )
	{
	if ( FIELD[x,8] < FIELD[x+1,8] )
		{
		FIELD[X+1,8]=FIELD[x+1,8] 
		FIELD[x+1,8]= FIELD[x,8] 
		FIELD[x,8]=FIELD[X+1,8]  
		FIELD[X+1,4]=FIELD[x+1,4]
		FIELD[x+1,4]= FIELD[x,4]
		FIELD[x,4]=FIELD[X+1,4]
		FIELD[X+1,6]=FIELD[x+1,6]
		FIELD[x+1,6]= FIELD[x,6]
		FIELD[x,6]=FIELD[X+1,6]
		FIELD[X+1,7]=FIELD[x+1,7]
		FIELD[x+1,7]= FIELD[x,7]
		FIELD[x,7]=FIELD[X+1,7]
		FIELD[X+1,9]=FIELD[x+1,9]
                FIELD[x+1,9]= FIELD[x,9]
                FIELD[x,9]=FIELD[X+1,9]
		x=0  			# reset to zero here sets x back to 1?
		}
	} 
for ( x=1 ; x < X ; x++ )
{
print "<TR VALIGN=TOP>"
print "<TD WIDTH=158><P ALIGN=CENTER>"FIELD[x,6]"</P></TD>"
print "<TD WIDTH=170><P ALIGN=CENTER>"FIELD[x,7]"</P></TD>"
print "<TD WIDTH=129><P ALIGN=CENTER>"FIELD[x,8]"</P></TD>"
print "<TD WIDTH=241><P ALIGN=CENTER STYLE=\"font-weight: medium\"><FONT SIZE=3>"
print "<A HREF=\"http:\/\/www-cde.ireland\/test\/"FIELD[x,4]"\/results\/"FIELD[x,6]"\/"FIELD[x,7]"\/"FIELD[x,8]"\/"FIELD[x,9]"\""">"FIELD[x,9]"</A></FONT></P>"
print "</TD>"
print "</TR>"
}
print "</TBODY>" 
print "</TABLE>" 
print "</CENTER>" 
print "<P ALIGN=CENTER><BR>" 
print "</P>" 
print "<P ALIGN=LEFT><BR><BR>" 
print "</P>" 
print "</BODY>"

}

