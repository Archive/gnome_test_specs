#!/usr/bin/nawk 
BEGIN{
FS="@@@"
Y=X=0 
HREF=HREF_L
HREF_TXT=LNK
}
# used by gnome_results.cgi and OO_results.cgi
{
if ($1 != "")                           		#ignore blank lines 
	{ 
	RESULT=$5 
	BUG=$6 
	if (MAJOR[X] != $2) 				# check if major component change 		
		{ MM="FALSE" 					# if change check all othe majors
		for (z=1 ; z <= X ; z++ )
			{
			if ( MAJOR[z] == $2) { TOTAL=X; MM="TRUE" ; X=z}
			}
		if (MM=="FALSE") 
			{ X++ ; TOTAL=X } 
		}
	MAJOR[X]=$2					# set major test component to current
	if ( MINOR[X,Y] != $3 ) {Y++} 
	MINOR[X,Y]=$3 
        LINK[X,Y]=$1
	if (RESULT == "N") { NCOUNT[X,Y]++ ;NCOUNT_T[X]++ } 
	if (RESULT == "P") { PCOUNT[X,Y]++ ;PCOUNT_T[X]++ }		
	if (RESULT == "F") { FCOUNT[X,Y]++ ;FCOUNT_T[X]++ }
	if (BUG    != "<TD><BR></TD></TR>" ) { BCOUNT[X,Y]++; BCOUNT_T[X]++}  
	}
} 
END{
print "<HTML><HEAD><TITLE>Test Summary</TITLE></HEAD>"
print "<BODY BGCOLOR=ffffff>"
print "<P>"
print "<CENTER><TABLE BORDER=2 CELLPADDING=2 CELLSPACING=2>"
print "<CAPTION><B>Test Summary  "HEADING"</B></CAPTION>"
print "<TR><TD>Major Test</TD>"
print "<TD>Minor Test </TD>"
print "<TD>Number of tests passed</TD>"
print "<TD>Number of tests failed</TD>"
print "<TD>Number of tests not run</TD>"
print "<TD>Number of Bugs Logged</TD></TR>"

for ( x = 1 ; x <= TOTAL ; x++)
	{
	printf "<TR><TD><BR></TD><TD><BR></TD><TD><BR></TD><TD><BR></TD><TD><BR></TD><TD><BR></TD></TR>"
	for ( y = 1 ; y <= Y ; y++)
		{
		if (MINOR[x,y] != "")
		printf "<TR><TD><P ALIGN=CENTER>%s</P></TD><TD><P ALIGN=CENTER><A HREF=\"%s\">%s</A></P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD></TR>\n",MAJOR[x], LINK[x,y], MINOR[x,y], PCOUNT[x,y], FCOUNT[x,y], NCOUNT[x,y], BCOUNT[x,y] 
		}
printf "<TR><TD><P ALIGN=CENTER>%s</P></TD><TD><P ALIGN=CENTER>Totals</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD></TR>\n",MAJOR[x], PCOUNT_T[x], FCOUNT_T[x], NCOUNT_T[x], BCOUNT_T[x]	
	}
print "</CENTER></TABLE>" 
print "</P>"
print "<P ALIGN=CENTER>"
print "<A HREF=\" "   HREF "  \" TARGET=\"_parent\"><B><FONT SIZE=5 STYLE=\"font-size: 20pt\"> "  HREF_TXT  "</FONT>" 
print "</B></A></P>" 
print "</BODY></HTML>"
}

