#!/bin/sh

#base_dir=".."
#dir="$base_dir/components"
#script="../../scripts/mk_toc_comp.pl"
# temp directory assignments
base_dir="/scde/web/docs/test/gnome_test"
dir="$base_dir/components"
script="$base_dir/scripts/mk_toc_comp.pl"


cd $dir

cd shakedown; $script shakedown "Shakedown"; cd ..
cd gnomecc; $script gnomecc "Control Center"; cd ..
cd xchat; $script xchat "X-Chat"; cd ..
cd sound_recorder; $script sound_recorder "Sound Recorder"; cd ..
cd sys_log; $script sys_log "System Log Monitor"; cd ..
cd system_info; $script system_info "System Information"; cd ..
cd user_listing; $script user_listing User Listing; cd ..
cd terminal; $script terminal Terminal; cd ..
cd eog; $script eog "Eye of Gnome"; cd ..
cd sys_monitor; $script sys_monitor "System Monitor"; cd ..
cd gcalc; $script gcalc Calculator; cd ..
cd gcharmap; $script gcharmap "Character Map"; cd ..
cd gcolorsel; $script gcolorsel "Color Selector"; cd ..
cd gfontsel; $script gfontsel "Font Selector"; cd ..
cd gsearchtool; $script gsearchtool "Search Tool"; cd ..
cd ghex; $script ghex "Hex Editor"; cd ..
cd gedit; $script gedit "gEdit"; cd ..
cd deskguide; $script deskguide "Deskguide Applet"; cd ..
cd printer; $script printer "Printer Applet"; cd ..
cd tasklist; $script tasklist "Tasklist Applet"; cd ..
cd char_pick_applet; $script char_pick_applet "Character Picker Applet"; cd ..
cd gnotes; $script gnotes "GNotes Applet"; cd ..
cd commander; $script commander "Mini Commander Applet"; cd ..
cd screenshooter; $script screenshooter "Screen Shooter"; cd ..
cd quicklaunch; $script quicklaunch "QuickLaunch Applet"; cd ..
cd another_clock_applet; $script another_clock_applet "Another Clock Applet"; cd ..
cd cdplayer_applet; $script cdplayer_applet "CDPlayer Applet"; cd ..
cd clock_applet; $script clock_applet "Clock Applet"; cd ..
cd clockmail; $script clockmail "Clock & Mailcheck Applet"; cd ..
cd dia; $script dia "Dia"; cd ..
cd gnome_about; $script gnome_about "Gnome About"; cd ..
cd lockscreen; $script lockscreen "Lockscreen"; cd ..
cd mailcheck; $script mailcheck "Mailcheck Applet"; cd ..
cd mixer; $script mixer "Mixer Applet"; cd ..
cd panel; $script panel "Gnome Panel"; cd ..
cd run; $script run "Run"; cd ..
cd startup_hints; $script startup_hints "Startup Hints"; cd ..
cd stripchart_applet; $script stripchart_applet "Stripchart Applet"; cd ..
cd textfile_viewer; $script textfile_viewer "Textfile Viewer"; cd ..
cd time_tracker; $script time_tracker "Time Tracker"; cd ..
cd geyes; $script geyes "gEyes"; cd ..
cd whereami_applet; $script whereami_applet "Whereami"; cd ..
cd keymap; $script keymap "Key Mappings";cd ..
cd gsession; $script gsession "Gnome-session"; cd ..
cd afterstep_clock_applet; $script afterstep_clock_applet "Afterstep Clock"; cd ..
cd takstat; $script takstat "Tick-a-Stat"; cd ..
cd weather_applet; $script weather_applet "Gnome Weather Applet"; cd ..
cd webcon; $script webcon "Web Control Applet"; cd ..
cd gstocktic; $script gstocktic "Gnome Stock Ticker Applet"; cd ..
cd gdict_applet; $script gdict_applet "gDict Applet"; cd ..
cd gdict; $script gdict; "gDict"; cd ..
cd ghostviewer; $script ghostviewer "Ghostviewer"; cd ..
cd menu_editor; $script menu_editor "Menu Editor"; cd ..
cd cdplayer; $script cdplayer "CD Player"; cd ..
cd xmms_applet; $script xmms_applet "XMMS Applet"; cd ..
cd xmms; $script xmms "XMMS"; cd ..
cd gfax; $script gfax "gFax"; cd ..
cd gdb; $script gdb "Gnome-db"; cd ..
cd scrollkeeper; $script scrollkeeper "scrollkeeper"; cd ..
cd glade; $script glade "Glade"; cd ..
cd sodipodi; $script sodipodi "Sodipodi"; cd ..
cd slashapp_applet; $script slashapp_applet "SlashApp Applet"; cd ..
cd battery_charge_monitor_applet; $script battery_charge_monitor_applet "Battery Charge Monitor Applet"; cd ..
cd gimp; $script gimp "GIMP"; cd ..
cd shutdown_reboot; $script shutdown_reboot "Shutdown & Reboot"; cd ..
