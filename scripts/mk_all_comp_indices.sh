#!/bin/sh

base_dir=".."
dir="$base_dir/components"
script="../../scripts/make_index.pl"

cd $dir

cd shakedown; $script components shakedown; chmod 755 index.html ;cd ..
cd gnomecc; $script components gnomecc; chmod 755 index.html ;cd ..
cd xchat; $script components xchat; chmod 755 index.html ; cd ..
cd sound_recorder; $script components sound_recorder; chmod 755 index.html ; cd ..
cd sys_log; $script components sys_log; chmod 755 index.html ; cd ..
cd system_info; $script components system_info; chmod 755 index.html ; cd ..
cd user_listing; $script components user_listing; chmod 755 index.html ; cd ..
cd terminal; $script components terminal; chmod 755 index.html ; cd ..
cd eog; $script components eog; chmod 755 index.html ; cd ..
cd sys_monitor; $script components sys_monitor; chmod 755 index.html ; cd ..
cd gcalc; $script components gcalc; chmod 755 index.html ; cd ..
cd gcharmap; $script components gcharmap; chmod 755 index.html ; cd ..
cd gcolorsel; $script components gcolorsel; chmod 755 index.html ; cd ..
cd gfontsel; $script components gfontsel; chmod 755 index.html ; cd ..
cd gsearchtool; $script components gsearchtool; chmod 755 index.html ; cd ..
cd ghex; $script components ghex; chmod 755 index.html ; cd ..
cd gedit; $script components gedit; chmod 755 index.html ; cd ..
cd deskguide; $script components deskguide; chmod 755 index.html ; cd ..
cd printer; $script components printer; chmod 755 index.html ; cd ..
cd tasklist; $script components tasklist; chmod 755 index.html ; cd ..
cd char_pick_applet; $script components char_pick_applet; chmod 755 index.html ; cd ..
cd gnotes; $script components gnotes; chmod 755 index.html ; cd ..
cd commander; $script components commander; chmod 755 index.html ; cd ..
cd screenshooter; $script components screenshooter; chmod 755 index.html ; cd ..
cd quicklaunch; $script components quicklaunch; chmod 755 index.html ; cd ..
cd another_clock_applet; $script components another_clock_applet; chmod 755 index.html ; cd ..
cd cdplayer_applet; $script components cdplayer_applet; chmod 755 index.html ; cd ..
cd clock_applet; $script components clock_applet; chmod 755 index.html ; cd ..
cd clockmail; $script components clockmail; chmod 755 index.html ; cd ..
cd dia; $script components dia; chmod 755 index.html ; cd ..
cd gnome_about; $script components gnome_about; chmod 755 index.html ; cd ..
cd lockscreen; $script components lockscreen; chmod 755 index.html ; cd ..
cd mailcheck; $script components mailcheck; chmod 755 index.html ; cd ..
cd mixer; $script components mixer; chmod 755 index.html ; cd ..
cd panel; $script components panel; chmod 755 index.html ; cd ..
cd run; $script components run; chmod 755 index.html ; cd ..
cd startup_hints; $script components startup_hints; chmod 755 index.html ; cd ..
cd stripchart_applet; $script components stripchart_applet; chmod 755 index.html ; cd ..
cd textfile_viewer; $script components textfile_viewer; chmod 755 index.html ; cd ..
cd time_tracker; $script components time_tracker; chmod 755 index.html ; cd ..
cd geyes; $script components geyes; chmod 755 index.html ; cd ..
cd whereami_applet; $script components whereami_applet; chmod 755 index.html ; cd ..
cd keymap; $script components keymap; chmod 755 index.html ; cd ..
cd gsession; $script components gsession; chmod 755 index.html ; cd ..
cd afterstep_clock_applet; $script components afterstep_clock_applet; chmod 755 index.html ; cd ..
cd takstat; $script components takstat; chmod 755 index.html ; cd ..
cd weather_applet; $script components weather_applet; chmod 755 index.html ; cd ..
cd webcon; $script components webcon; chmod 755 index.html ; cd ..
cd gstocktic; $script components gstocktic; chmod 755 index.html ; cd ..
cd gdict_applet; $script components gdict_applet; chmod 755 index.html ; cd ..
cd gdict; $script components gdict; chmod 755 index.html; cd ..
cd ghostviewer; $script components ghostviewer; chmod 755 index.html ; cd ..
cd menu_editor; $script components menu_editor; chmod 755 index.html ; cd ..
cd cdplayer; $script components cdplayer; chmod 755 index.html ; cd ..
cd xmms_applet; $script components xmms_applet; chmod 755 index.html ; cd ..
cd xmms; $script components xmms; chmod 755 index.html ; cd ..
cd gfax; $script components gfax; chmod 755 index.html; cd ..
cd gdb; $script components gdb; chmod 755 index.html; cd ..
cd scrollkeeper; $script components scrollkeeper; chmod 755 index.html; cd ..
cd glade; $script components glade; chmod 755 index.html; cd ..
cd sodipodi; $script components sodipodi; chmod 755 index.html; cd ..
cd slashapp_applet; $script components slashapp_applet; chmod 755 index.html; cd ..
cd battery_charge_monitor_applet; $script components battery_charge_monitor_applet; chmod 755 index.html; cd ..
cd gimp; $script components gimp; chmod 755 index.html; cd ..
cd shutdown_reboot; $script components shutdown_reboot; chmod 755 index.html; cd ..
